﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audire_POCO
{
    public class DashboardPOCO
    {
        public string CssClass { get; set; }
        public string Module_id { get; set; }
        public string Module_name { get; set; }
        public string auditType_id { get; set; }
        public DashboardDetailPOCO detail { get; set; }
        
    }

    public class PendingAuditsPOCO {
        public string planned_date { get; set; }
        public string plan_week { get; set; }
        public int location_id { get; set; }
        public string location { get; set; }
        public int AuditType_id { get; set; }
        public string AuditType { get; set; }
        public int color_code { get; set; }
    }
    public class DashboardDetailPOCO
    {
        public List<AuditTypePOCO> AuditScores { get; set; }
        public string Average_MonthlyScore { get; set; }
        public string Average_QuarterlyScore { get; set; }
        public string Average_HalfYearlyScore { get; set; }
        public string Average_YearlyScore { get; set; }
        public string relativeScore { get; set; }
        public string scheduled_date { get; set; }
        public string Updated_date { get; set; }
        public string total_Audits { get; set; }
        public string Open_NC { get; set; }
        public string Closed_NC { get; set; }
        public string PendingAudits { get; set; }
        public List<AuditTasks> Tasks { get; set; }
        public List<PendingAuditsPOCO> upcomingAudits { get; set; }
    }
    public class AuditTypePOCO
    {
        public int AuditType_id { get; set; }
        public string AuditType_name { get; set; }
        public string Audit_date { get; set; }
        public string Audit_Score { get; set; }
        public string Audit_location { get; set; }
    }
    public class AuditTasks {
        public string audit_id { get; set; }
        public string Audit_Type { get; set; }
        public string module { get; set; }
        public string location_name { get; set; }
        public string audit_performed_date { get; set; }
    }
    public class DashboardChart {
        public int audit_count { get; set; }
        public string audit_date { get; set; }
    }
    public class DashboardScore {
        public string average_score { get; set; }
        
    }
}
