﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audire_POCO
{
    public class ReportPOCO
    {
        public int audit_id { get; set; }
        public string audit_number { get; set; }
        public string Location { get; set; }
        public string Auditor { get; set; }
        public string Audit_date { get; set; }
    }
    public class ParamPOCO
    {
        public int user_id { get; set; }
        public int module_id { get; set; }
        public int subAudit_id { get; set; }
        public string subAudit_name { get; set; }

    }
    public class CAPA_POCO
    {
        public int audit_id { get; set; }
        public string Audit_number { get; set; }
        public int question_id { get; set; }
        public string audit_comment { get; set; }
        public string clause { get; set; }
        public string root_cause { get; set; }
        public string review_comment { get; set; }
        public string Corrective_Action { get; set; }
        public string Preventive_Action { get; set; }
        public string target_date { get; set; }
        public string Process { get; set; }
        public string Status { get; set; }
        public int ID { get; set; }
        public string SubAudit_Name { get; set; }
        public int review_status { get; set; }
        public string confirm_comment { get; set; }
        public string report_date { get; set; }
        public string containment { get; set; }
    }

    public class SubAudit
    {
        public int ID { get; set; }
        public string SubAudit_Name { get; set; }
        public string div_class { get; set; }
    }

    public class Confirmation
    {
        public int audit_id { get; set; }
        public int question_id { get; set; }
        public string confirm_comment { get; set; }
        public int confirmed_by { get; set; }
        public int err_code { get; set; }
        public string err_msg { get; set; }
    }

    public class AuditStatus
    {
        public string module { get; set; }
        public string Audit_Type { get; set; }
        public DateTime? audit_performed_date { get; set; }
        public string emp_full_name { get; set; }
        public string Audit_number { get; set; }
        public string audit_id { get; set; }
        public string status { get; set; }
    }
}
