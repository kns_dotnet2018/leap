﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audire_POCO
{
    public class AuditPOCO
    {
    }
    public class PlanRatePOCO
    {
        public string TotalPlan { get; set; }
        public string LMTotalPlan { get; set; }
        public string TotalPlanRate { get; set; }
        public string TotalPlanCompleted { get; set; }
        public string LMTotalCompleted { get; set; }
        public string TotalCompletedRate { get; set; }

    }

    public class AuditCount
    {
        public int auditid { get;set; }
        public string TotalauditCount { get; set; }
        public string NCQuestionCount { get; set; }
        public string CQuestionCount { get; set; }
        public string colorlist { get; set; }
    }
    public class colorclass
    {
        public string color { get; set; }
    }
}
