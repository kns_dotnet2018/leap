﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using System.Data;
using Audire_EF;
using Audire_MVC.Models;
using Audire_POCO;
using System.Globalization;
using System.Drawing;
using System.IO;
using QRCoder;
using Audire_MVC.Utilities;


namespace Audire_MVC.Controllers
{

    public class BulkQRDetailsController : Controller
    {
        //
        // GET: /BulkQRDetails/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        private string location_id;
        public ActionResult BulkQRDetails()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])) && (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Admin))))
            {
                //Session["ddlLocation"] = objModel.getLocationList();
                //Session["ddlRegion"] = objModel.getRegionList();
                //Session["ddlCountry"] = objModel.getCountryList();
                //Session["ddlLine"] = objModel.getLineList();
                Session["ddlProcess"] = objModel.getProcessAuditList();
                List<ProcesssPOCO> listBulkQR = new List<ProcesssPOCO>();
               // listBulkQR = (List<ProcesssPOCO>)Session["ddlProcess"];
                return View(listBulkQR);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }
        public PartialViewResult DisplayBulkQRCode(int process_id, int module_id)
        {
            List<ProcesssPOCO> listBulkQR = new List<ProcesssPOCO>();
            listBulkQR = (List<ProcesssPOCO>)Session["ddlProcess"];
            if (process_id > 0)
            {
                var QR = (from s in listBulkQR.AsEnumerable()
                          where s.id == process_id && s.module_id==module_id
                          select s).ToList();

                return PartialView(QR);
            }
            else
            {
                var QR = (from s in listBulkQR.AsEnumerable()
                          where s.module_id == module_id
                          select s).ToList();

                return PartialView(QR);
                //return PartialView(listBulkQR);
            }
        }
        public ActionResult GetQRCode(int id)
        {
            int line_id = id;
            string code = Convert.ToString(line_id);
            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
            using (Bitmap bitMap = qrCode.GetGraphic(20))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                    return File(byteImage, "image/png");
                }
            }
        }


        public ActionResult MobileBulkQRDetails(string userId)
        {
            List<ProcesssPOCO> listBulkQR = new List<ProcesssPOCO>();
            CommonDropdowns objCom = new CommonDropdowns();
            if (!string.IsNullOrEmpty(userId))
            {
                objCom.SetSessionForMobileUser(userId);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
                {
                    Session["ddlProcess"] = objModel.getProcessAuditList();

                    listBulkQR = (List<ProcesssPOCO>)Session["ddlProcess"];
                }
                
            }
            return View(listBulkQR);
        }

    //    public ActionResult Pdf(/* some input arguments */)
    //   {
    //var htmlString = GetHtmlToConvert(...);
 
    //var fullFileName = "";    
    //try
    //{
    //    var baseUrl = Request.Url.GetLeftPart(UriPartial.Authority);
    //    fullFileName = Server.MapPath(
    //        UrlHelper.GenerateContentUrl("~/assets/yourfile.pdf), HttpContext));
    //    PdfService.Create(htmlString, fullFileName, baseUrl);
    //}
    //catch(Exception ex)
    //{
    //    return Content(ex.Message);
    //}
 
    //return File(fullFileName, "application/pdf", "yourfile.pdf");
    //}


        //public ActionResult QRDetails(int process_id)
        //{
        //    if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
        //    {

        //        return View(QR);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login", "User");
        //    }
        //}
    }
}
