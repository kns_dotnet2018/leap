﻿using Audire_EF;
using Audire_MVC.Models;
using Audire_MVC.Utilities;
using Audire_POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Audire_MVC.Controllers
{
    public class SectorDashboardController : Controller
    {
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        //
        // GET: /SectorDashboard/

        /// <summary>
        /// author:Monika M S
        /// date:2020-09-25
        /// </summary>
        /// <returns>Get the Home Screen</returns>
        public ActionResult Index()
        {
            List<SectorPOCO> list = new List<SectorPOCO>();
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Session["LoginID"])))
                {
                    var sectorDet = datacontext.tbl_sector_master.ToList();
                    foreach (var secrow in sectorDet)
                    {
                        SectorPOCO obj = new SectorPOCO();
                        if (secrow.ID == 1)
                            obj.cssClass = "box-part box-part2 text-center";
                        else if (secrow.ID == 2)
                            obj.cssClass = "box-part text-center";
                        else if (secrow.ID == 3)
                            obj.cssClass = "box-part box-part1 text-center";
                        else
                            obj.cssClass = "panel-featured-left panel-featured-four";

                      
                        obj.ID = secrow.ID;
                        obj.sector_name = secrow.Sector_Name;
                        list.Add(obj);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "User");
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return View(list);
        }

        /// <summary>
        /// author:Monika M S
        /// date:2020-09-28
        /// </summary>
        /// <param name="sectorid"></param>
        /// <returns>Validating whether user is of perticular sector or not</returns>
        public string CheckValidUser(int sectorid)
        {
            string res = "";
            string lstempdata = "";
            List<ModulePOCO> modulelist = new List<ModulePOCO>();
            tbl_user_master login_user = new tbl_user_master();
            login_user.username = Convert.ToString(HttpContext.Session["LoginUsername"]);
            login_user.passwd = Convert.ToString(HttpContext.Session["UserPassword"]);

            if (!string.IsNullOrEmpty(login_user.username) && !string.IsNullOrEmpty(login_user.passwd))
            {
                login_user.Sector = Convert.ToString(sectorid);

                lstempdata  = objModel.ValidateUserData(login_user);

                if (lstempdata != "")
                {
                    HttpContext.Session["SectorId"] = sectorid;
                    HttpContext.Session["SectorName"] = datacontext.tbl_sector_master.Where(m => m.ID == sectorid).Select(m => m.Sector_Name).FirstOrDefault();
                    string sectorname = Convert.ToString(HttpContext.Session["SectorName"]);
                    //string[] sec = lstempdata.Split(',');
                    //foreach (var row in sec)
                    //{
                    //    int secid = Convert.ToInt32(row);
                    //    var moduledata = datacontext.tbl_Module_master.Where(p => p.Sector == secid).ToList();
                    //    foreach (var modrow in moduledata)
                    //    {
                    //        ModulePOCO objsec = new ModulePOCO();
                    //        objsec.module_id = modrow.module_id;
                    //        objsec.module = modrow.module;
                    //        modulelist.Add(objsec);
                    //    }
                    //}

                    var result1 = datacontext.tbl_Module_master.Where(m => m.status == 1 && m.Sector == sectorid).
                                   AsEnumerable().Select(c => new ModulePOCO
                                   {
                                       module = c.module,
                                       module_id = c.module_id
                                   }).ToList();

                    Session["ddlModule"] = result1;
                    if (sectorid != 1 )
                    {
                        res = "Others";
                    }
                    if (sectorid != 1 && sectorid != 3)
                    {
                        res = "Others";
                    }
                    else if (sectorid == 3)
                    {
                        res = "Sustainability";
                    }
                    else
                    {
                        res = "Success";
                    }
                }
                else
                {
                    res = "You Do not have the access";
                }

            }
            else
            {
                res = "fail";
            }

            return res;
        }

    }
}
