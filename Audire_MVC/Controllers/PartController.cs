﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using System.Data;
using Audire_EF;
using Audire_MVC.Models;
using Audire_POCO;
using System.Data.OleDb;
using System.IO;
using Audire_MVC.Utilities;

namespace Audire_MVC.Controllers
{
    public class PartController : Controller
    {
        //
        // GET: /Part/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();

        public string p_add_edit_flag, p_parts_table;

        public ActionResult Part()
        {
            List<PartPOCO> listPart = new List<PartPOCO>();
            try
            {

                if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
                {
                    Session["ddlLocation"] = objModel.getLocationList();
                    Session["ddlRegion"] = objModel.getRegionList();
                    Session["ddlCountry"] = objModel.getCountryList();
                    Session["ddlLine"] = objModel.getLineList();
                    listPart = objModel.GetProduct(Convert.ToInt32(Session["LoginID"]));
                    return View(listPart);
                }
                else
                {
                    return RedirectToAction("Login", "User");
                }

            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return View(listPart);
            }
        }

        public PartialViewResult EditPart(string product_id)
        {
            List<PartPOCO> listPart = new List<PartPOCO>();
            try
            {

                listPart = objModel.GetProduct(Convert.ToInt32(Session["LoginID"]));
                var part = (from s in listPart.AsEnumerable()
                            where s.product_id == Convert.ToInt32(product_id)
                            select s).ToList();
                Session["Add_Edit_Flag"] = "Edit";
                p_add_edit_flag = Convert.ToString(Session["Add_Edit_Flag"]);

                return PartialView("EditPart", part);

            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return PartialView("EditPart", listPart);
            }
        }

        public PartialViewResult DisplayPart(string Id, string viewName)
        {
            List<PartPOCO> listpart = new List<PartPOCO>();
            try
            {
                listpart = objModel.GetProduct(Convert.ToInt32(Session["LoginID"]));
                if (string.IsNullOrEmpty(Id) || Id == "0")
                {
                    var part = (from s in listpart.AsEnumerable()
                                select s
                                ).ToList();
                    return PartialView("DisplayPart", part[0]);
                }
                else
                {
                    var part = (from s in listpart.AsEnumerable()
                                where s.product_code == Convert.ToString(Id.Trim())
                                select s).ToList();
                    return PartialView(viewName, part);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return PartialView(listpart);
            }
        }

        public PartialViewResult AddNewPart()
        {
            PartPOCO ObjPart = new PartPOCO();
            List<PartPOCO> partList = new List<PartPOCO>();
            try
            {
                partList.Add(ObjPart);
                Session["Add_Edit_Flag"] = "Add";
                return PartialView("EditPart", partList);
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return PartialView(partList);
            }
        }

        public JsonResult GetCountries(int region_id)
        {
            try
            {
                var dbCountries = datacontext.tbl_country_master.ToList();
                var countries = (from m in dbCountries
                                 where m.region_id == (region_id == 0 ? m.region_id : region_id)
                                 select new
                                 {
                                     m.country_id,
                                     m.country_name
                                 }
                               );
                return Json(countries, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return Json(true);
            }
        }

        public JsonResult GetLocations(int country_id)
        {
            try{
            var dbLocations = datacontext.tbl_location_master.ToList();
            var locations = (from m in dbLocations
                             where m.country_id == (country_id == 0 ? m.country_id : country_id)
                             select new
                             {
                                 m.location_id,
                                 m.location_name
                             });
            return Json(locations, JsonRequestBehavior.AllowGet);
             }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return Json(true);
            }
        }

        public PartialViewResult UploadExcel(string CurrentFilePath)
        {

            //if (updFile.PostedFile != null && updFile.PostedFile.ContentLength > 0)
            //{

            //string fileName = Path.GetFileName(updFile.PostedFile.FileName);
            string folder = Server.MapPath("~/Reports/");
            Directory.CreateDirectory(folder);
            CurrentFilePath = Path.Combine(folder);
            // updFile.PostedFile.SaveAs(Path.Combine(folder, fileName));
            try
            {
                InsertExcelRecords(CurrentFilePath);
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            // }
            return PartialView("EditPart");
        }

        private void InsertExcelRecords(string CurrentFilePath)
        {
            try
            {
                PartPOCO objpart = new PartPOCO();
                int output;
                List<PartPOCO> partlist = new List<PartPOCO>();
                OleDbConnection Econ;
                string constr = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES;""", CurrentFilePath);
                Econ = new OleDbConnection(constr);
                string Query = string.Format("Select [Region],[Country],[Location],[LineName],[PartNumber] FROM [{0}]", "Part_Mass_Upload$");
                OleDbCommand Ecom = new OleDbCommand(Query, Econ);
                //DataSet ds = new DataSet();
                //OleDbDataAdapter oda = new OleDbDataAdapter(Query, Econ);
                ////Econ.Close();
                //oda.Fill(ds);
                p_add_edit_flag = Convert.ToString(Session["Add_Edit_Flag"]);
                DataTable Exceldt = new DataTable();
                Exceldt.Columns.Add("region", typeof(string));
                Exceldt.Columns.Add("country", typeof(string));
                Exceldt.Columns.Add("location", typeof(string));
                Exceldt.Columns.Add("lineName", typeof(string));
                Exceldt.Columns.Add("PartNumber", typeof(string));
                DataRow dr = Exceldt.NewRow();
                dr["region"] = Convert.ToString(Request.Form["Region"]);
                dr["country"] = Convert.ToString(Request.Form["Country"]);
                dr["location"] = Convert.ToString(Request.Form["Location"]);
                dr["lineName"] = Convert.ToString(Request.Form["LineName"]);
                dr["PartNumber"] = Convert.ToString(Request.Form["PartNum"]);
                output = objModel.SaveProduct(p_add_edit_flag, Exceldt);
                partlist.Add(objpart);
                TempData["Partmessage"] = "There is error in saving parts. Please check all the columns added in excel sheet and try again..";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult SavePart(FormCollection fc)
        {
            PartPOCO objPartPOCO = new PartPOCO();
            try
            {
                int output;
                p_add_edit_flag = Convert.ToString(Session["Add_Edit_Flag"]);
                DataTable dt = new DataTable();
                dt.Columns.Add("region", typeof(string));
                dt.Columns.Add("country", typeof(string));
                dt.Columns.Add("location", typeof(string));
                dt.Columns.Add("lineName", typeof(string));
                dt.Columns.Add("PartNumber", typeof(string));
                DataRow dr = dt.NewRow();
                //foreach (Object form in fc)
                //{
                dr["region"] = Convert.ToString(Request.Form["Region"]);
                dr["country"] = Convert.ToString(Request.Form["Country"]);
                dr["location"] = Convert.ToString(Request.Form["Location"]);
                dr["lineName"] = Convert.ToString(Request.Form["LineName"]);
                dr["PartNumber"] = Convert.ToString(Request.Form["PartNum"]);
                dt.Rows.Add(dr);
                output = objModel.SaveProduct(p_add_edit_flag, dt);
                if (output > 0)
                {
                    //tbl_product_master tblpart = new tbl_product_master();
                    //tblpart = datacontext.tbl_product_master.AsQueryable().
                    //    FirstOrDefault(k => k.product_name == Convert.ToString(objPartPOCO.product_name));

                    TempData["Partmessage"] = "Part details are updated successfully.";
                }

            }
            //}
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                TempData["Partmessage"] = "Error in updating the part details. Please try again.";
                ex.ToString();
            }
            return RedirectToAction("Part", "Part");
        }
    }
}
