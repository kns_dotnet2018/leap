﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using System.Data;
using Audire_EF;
using Audire_MVC.Models;
using Audire_POCO;
using System.Globalization;
using System.Drawing;
using System.IO;
using QRCoder;
using System.Web.Services;

namespace Audire_MVC.Controllers
{
    public class LineController : Controller
    {
        //
        // GET: /Line/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        public ActionResult Line()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
            {
            Session["ddlLocation"] = objModel.getLocationList();
            Session["ddlRegion"] = objModel.getRegionList();
            Session["ddlCountry"] = objModel.getCountryList();
            List<LinePOCO> listLine = new List<LinePOCO>();
            listLine = objModel.getLineList();
            return View(listLine);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }
        public PartialViewResult EditLine(int line_id)
        {
           
            List<LinePOCO> listLine = new List<LinePOCO>();
            listLine = objModel.getLineList();
            var line = (from s in listLine.AsEnumerable()
                        where s.line_id == Convert.ToInt32(line_id)
                        select s).ToList();
          
            return PartialView("EditLine",line);
        }
        public PartialViewResult DisplayLine(string Id, string viewName)
        {
            List<LinePOCO> listLine = new List<LinePOCO>();
            listLine = objModel.getLineList();
            if (string.IsNullOrEmpty(Id) || Id == "0")
            {
                var line = (from s in listLine.AsEnumerable()
                            select s
                            ).ToList();
                return PartialView("DisplayLine", line[0]);
              
            }
            else
            {
                var line = (from s in listLine.AsEnumerable()
                            where s.line_id == Convert.ToInt32(Id.Trim())
                            select s).ToList();
                return PartialView(viewName, line);
               
            }

            //return PartialView("DisplayLine");
        }
        public PartialViewResult AddNewLine()
        {
            LinePOCO ObjLine = new LinePOCO();
            List<LinePOCO> lineList = new List<LinePOCO>();
            lineList.Add(ObjLine);
            return PartialView("EditLine", lineList);
        }
        public JsonResult GetDistributionList(int? location_id)
        {
            var dbDistribution = datacontext.tbl_user_master.ToList();
            var Distribution = (from m in dbDistribution
                                where m.location_id == (location_id == 0?m.location_id:location_id)
                             select new
                             {
                                 m.user_id,
                                 m.email_id
                             }
                           );
            return Json(Distribution, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult PrintQRCode()
        {
            LinePOCO ObjLine = new LinePOCO();
            List<LinePOCO> lineList = new List<LinePOCO>();
            lineList.Add(ObjLine);
            return PartialView("EditLine", lineList);
        }
        public JsonResult GetCountries(int region_id)
        {
            var dbCountries = datacontext.tbl_country_master.ToList();
            var countries = (from m in dbCountries
                             where m.region_id == (region_id == 0 ? m.region_id : region_id)
                             select new
                             {
                                 m.country_id,
                                 m.country_name
                             }
                           );
            return Json(countries, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLocations(int country_id)
        {
            var dbLocations = datacontext.tbl_location_master.ToList();
            var locations = (from m in dbLocations
                             where m.country_id == (country_id == 0 ? m.country_id : country_id)
                             select new
                             {
                                 m.location_id,
                                 m.location_name
                             });
            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLine()
        {
            LinePOCO objLinePOCO = new LinePOCO();

            try
            {
                objLinePOCO.region_id = String.IsNullOrEmpty(Convert.ToString(Request["m.region"])) ? 0 : Convert.ToInt32(Request["m.region"]);
                objLinePOCO.country_id = String.IsNullOrEmpty(Convert.ToString(Request["m.country"])) ? 0 : Convert.ToInt32(Request["m.country"]);
                objLinePOCO.location_id = String.IsNullOrEmpty(Convert.ToString(Request["m.location"])) ? 0 : Convert.ToInt32(Request["m.location"]);
                objLinePOCO.line_name = Request["m.line_name"];
                objLinePOCO.line_id = Convert.ToInt32(Request["m.line_id"]);
                objLinePOCO.distribution_list = Request["Dist"];
                objLinePOCO.Module_id = Request["Module"];
                if (objLinePOCO.line_id > 0 && objLinePOCO.location_id > 0 && objLinePOCO.region_id > 0 && objLinePOCO.country_id>0)
                    {
                        tbl_line_master tblline = new tbl_line_master();
                        tblline = datacontext.tbl_line_master.AsQueryable().
                            FirstOrDefault(k => k.line_id == objLinePOCO.line_id);

                        if (tblline != null)
                        {
                            tblline.region_id = objLinePOCO.region_id;
                            tblline.country_id = objLinePOCO.country_id;
                            tblline.location_id = objLinePOCO.location_id;
                            tblline.line_name = objLinePOCO.line_name;
                            tblline.line_id = objLinePOCO.line_id;
                            tblline.distribution_list = objLinePOCO.distribution_list;
                            tblline.module_id = Convert.ToString(objLinePOCO.Module_id);
                            datacontext.SaveChanges();
                           // datacontext.tbl_line_master.Add(tblline);
                            TempData["Linemessage"] = "Line details are updated successfully.";
                        }
                        else
                        {
                            TempData["Linemessage"] = "No line is found for update. Please try again.";
                        }
                    }
                
                else
                {
                    if (objLinePOCO.location_id > 0 && objLinePOCO.region_id > 0 && objLinePOCO.country_id > 0)
                    {
                        tbl_line_master tblline = new tbl_line_master();
                        tblline.region_id = objLinePOCO.region_id;
                        tblline.country_id = objLinePOCO.country_id;
                        tblline.location_id = objLinePOCO.location_id;
                        tblline.line_name = objLinePOCO.line_name;
                        tblline.line_id = objLinePOCO.line_id;
                        tblline.distribution_list = objLinePOCO.distribution_list;
                        tblline.module_id = Convert.ToString(objLinePOCO.Module_id);
                        tblline.start_date = DateTime.Now;
                        datacontext.tbl_line_master.Add(tblline);
                        datacontext.SaveChanges();
                        TempData["Linemessage"] = "Line details are saved successfully.";
                    }
                    else
                    {
                        TempData["Linemessage"] = "Region, Country and Location need to be selected while saving line details.";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Linemessage"] = "Error in updating the line details. Please try again.";
                ex.ToString();
            }
            return RedirectToAction("Line", "Line");
        }

        public ActionResult GetQRCode(int line_id)
        {
            string code = Convert.ToString(line_id);
            System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
            //imgBarCode.Height = 150;
            //imgBarCode.Width = 150;
            //imgBarCode.ID = "image";
            using (Bitmap bitMap = qrCode.GetGraphic(20))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    //imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                    return File(byteImage, "image/png");
                }
            }



            //string base64String = string.Empty;
            //byte[] bDoccontent;
            //byte[] byteArray;
            //byteArray = Encoding.UTF8.GetBytes(base64String);
            //bDoccontent = imgBarCode;
            //base64String = Convert.ToBase64String(bDoccontent, 0, bDoccontent.Length);
            //byteArray = Encoding.UTF8.GetBytes(base64String);
            //Stream obj = new MemoryStream(byteArray);
            //StreamReader sr = new StreamReader(obj);
            //byte[] buffer = Convert.FromBase64String(sr.ReadToEnd());
            //return File(buffer, "image/jpeg");
        }

        [WebMethod]
        public static List<string> GetDistributionEmails(string Name)
        {
            List<string> empResult = new List<string>();

            return empResult;

        }




    }

}

