﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_POCO;
using Audire_MVC.Models;
using System.Configuration;
using Audire_MVC.Utilities;

namespace Audire_MVC.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/
        #region GlobalDeclaration
        MastersModel objModel = new MastersModel();
        #endregion
        public ActionResult AuditReport()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
            {

                List<ReportPOCO> listAudit = new List<ReportPOCO>();
                ParamPOCO param = new ParamPOCO();
                param.module_id = Convert.ToInt32(Session["module"]);
                param.user_id = Convert.ToInt32(Session["LoginID"]);
                listAudit = objModel.getAuditReport(param);
                return View(listAudit);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }
        //[HttpPost]
        //public JsonResult DownloadReport()
        //{
        //    ErrorOutput objerr = new ErrorOutput();
        //    try
        //    {
        //        int auditid = 23111;
        //        string filename = "Audit_Result_" + Convert.ToString(auditid) + ".pdf";
        //        string filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
        //        string completepath = filepath + filename;
        //        if (System.IO.File.Exists(completepath))
        //        {
        //            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        //            response.ClearContent();
        //            response.Clear();
        //            response.ContentType ="application/pdf";
        //            response.AddHeader("Content-Disposition",
        //                               "attachment; filename=" + Convert.ToString(filename) + ";");
        //            response.TransmitFile(Convert.ToString(completepath));
        //            response.Flush();
        //            response.End();
        //        }
        //        else
        //        {
        //            filepath = ConfigurationManager.AppSettings["PDF_Folder_Service"].ToString();
        //            completepath = filepath + filename;
        //            if (File.Exists(completepath))
        //            {
        //                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        //                response.ClearContent();
        //                response.Clear();
        //                response.ContentType = "text/plain";
        //                response.AddHeader("Content-Disposition",
        //                                   "attachment; filename=" + Convert.ToString(completepath) + ";");
        //                response.TransmitFile(Convert.ToString(completepath));
        //                response.Flush();
        //                response.End();
        //            }
        //            else
        //            {
        //                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Selected audit report is not available in storage.');", true);
        //            }
        //        }
        //        objerr.err_message = "Report is downloaded successfully.";
        //        objerr.err_code = 200;
        //    }
        //    catch (Exception ex)
        //    {
        //        objerr.err_message = ex.Message;
        //        objerr.err_code = 202;
        //        CommonFunctions.ErrorLog(ex);
        //    }
        //    return Json(objerr, JsonRequestBehavior.AllowGet);
        //}
        
        [HttpGet]
        public ActionResult DownloadFile(int audit_id)
        {
            string filename = "Audit_Result_" + Convert.ToString(audit_id) + ".pdf";
            string filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
            string completepath = filepath + filename;
            if (System.IO.File.Exists(completepath))
            {
                return File(completepath, "text/plain", filename);
            }
            else
            {
                TempData["reportmessage"] = "File Not Found.";
                return RedirectToAction("AuditReport","Report");
            }
        }

        public ActionResult JitsiMeet()
        {
            return View();
        }

        }
}
