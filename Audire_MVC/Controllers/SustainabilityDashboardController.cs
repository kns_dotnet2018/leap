﻿using Audire_EF;
using Audire_MVC.Models;
using Audire_MVC.Utilities;
using Audire_POCO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Audire_MVC.Controllers
{
    public class SustainabilityDashboardController : Controller
    {
        //
        // GET: /SustainabilityDashboard/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        /// <summary>
        /// Author:Monika M S
        /// Date:2020-12-17
        /// Desc:To get the dashboard screen
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Session["LoginID"])))
                {
                    int sectorid = Convert.ToInt32(HttpContext.Session["SectorId"]);
                    var fredet = datacontext.tbl_frequency_master.Where(m => m.FreaquencyId == 1).Select(m => m.Name).FirstOrDefault();
                    ViewBag.Frequency = new SelectList(datacontext.tbl_frequency_master, "FreaquencyId", "Name", fredet).ToList();
                    ViewBag.Location = new SelectList(objModel.getSiteNameList(), "location_id", "location").ToList();
                    using (Audire_dbEntities datacontext1 = new Audire_dbEntities())
                    {
                        ViewBag.Building = new SelectList(datacontext1.tbl_building_master, "BuildingID", "BuildingName").ToList();
                        ViewBag.Equipment = new SelectList(datacontext1.tbl_Module_master.Where(m => m.status == 1 && m.Sector == sectorid), "module_id", "module").ToList();
                        ViewBag.Asset = new SelectList(datacontext1.tbl_audit_type, "ID", "Audit_Type").ToList();
                    }

                }
                else
                {
                    return RedirectToAction("Login", "User");
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return View();
        }
        /// <summary>
        /// Author:Monika M S
        /// Date:2020-12-15
        /// Desc:To get the count of Total audit and to get audit status
        /// </summary>
        /// <param name="moduleid"></param>
        /// <param name="subauditId"></param>
        /// <returns></returns>
        public JsonResult getCountOfAudit(int moduleid,int subauditId)
        {
            List<AuditCount> objlist = new List<AuditCount>();
            try
            {
                var TotalauditCount = datacontext.tbl_AuditResultForAnswer.Where(m => m.module_id == moduleid && m.AuditType_id == subauditId && m.IsCompleted == "Y").Select(m => m.audit_id).Distinct().ToList();
                string count = TotalauditCount.Count().ToString();
                string NCQuestionCount = datacontext.tbl_AuditResultForAnswer.Where(m => m.module_id == moduleid && m.AuditType_id == subauditId && m.NCFlag == 1 && m.IsCompleted == "Y").Select(m => m.question_id).Count().ToString();
                string CQuestionCount = datacontext.tbl_AuditResultForAnswer.Where(m => m.module_id == moduleid && m.AuditType_id == subauditId && m.NCFlag == 0 && m.IsCompleted == "Y").Select(m => m.question_id).Count().ToString();

                for (int i = 0; i < TotalauditCount.Count; i++)
                {
                    int id = TotalauditCount[i];
                    AuditCount obj = new AuditCount();
                    var NCQuestionCount1 = datacontext.tbl_AuditResultForAnswer.Where(m => m.audit_id == id && m.NCFlag == 1).Count();
                    if (NCQuestionCount1 >= 1)
                    {

                        obj.colorlist = "Red";
                    }
                    else
                    {
                        obj.colorlist = "Green";
                    }
                    obj.auditid = id;
                    obj.TotalauditCount = count;
                    obj.NCQuestionCount = NCQuestionCount;
                    obj.CQuestionCount = CQuestionCount;
                    objlist.Add(obj);
                }
            }
            catch(Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
           
            return Json(objlist, JsonRequestBehavior.AllowGet);
        }


    }
}
