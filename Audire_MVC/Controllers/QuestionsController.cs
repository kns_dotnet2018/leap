﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using System.Data;
using Audire_EF;
using Audire_MVC.Models;
using Audire_POCO;
using Audire_MVC.Utilities;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using System.IO;
using OfficeOpenXml;
using System.Data.OleDb;
using System.Configuration;

namespace Audire_MVC.Controllers
{
    public class QuestionsController : Controller
    {
        //
        // GET: /Questions/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        public ActionResult Questions()
        {
            Session["Successmsg"] = null;
            if (Session["ImportExcel"] == null)
            {
                Session["ImportExcel"] = "import";
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])) && (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Admin))))
            {
                List<QuestionsPOCO> listQuestions = new List<QuestionsPOCO>();
                var fredet = datacontext.tbl_frequency_master.Where(m => m.FreaquencyId == 1).Select(m => m.Name).FirstOrDefault();
                ViewBag.Frequency = new SelectList(datacontext.tbl_frequency_master, "FreaquencyId", "Name", fredet).ToList();
                return View(listQuestions);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }
        [HttpPost]
        public ActionResult DeleteQuestion(string module_id, string subAudit, string question_id)
        {
            OutputPOCO objOutput = new OutputPOCO();
            tbl_question_master tblQuest = new tbl_question_master();
            try
            {
                int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                int q_id = Convert.ToInt32(question_id);
                int m_id = Convert.ToInt32(module_id);
                if (Convert.ToInt32(HttpContext.Session["SectorId"]) != 1)
                {
                    tbl_Question_TypeWithJson tblQuest1 = new tbl_Question_TypeWithJson();
                    var id1 = datacontext.tbl_Question_TypeWithJson.Where(k => k.Question_Id == q_id && k.Audit_type_id == subAuditType_id && k.module_id == m_id).FirstOrDefault();
                    datacontext.Entry(id1).State = EntityState.Deleted;
                    datacontext.SaveChanges();
                    tblQuest1 = datacontext.tbl_Question_TypeWithJson.Where(k => k.Audit_type_id == subAuditType_id && k.module_id == m_id).FirstOrDefault();
                    if (tblQuest1 == null)
                    {
                        var type_id = datacontext.tbl_audit_type.Where(k => k.ID == subAuditType_id && k.module_id == m_id).FirstOrDefault();

                        datacontext.Entry(type_id).State = EntityState.Deleted;
                        try
                        {
                            datacontext.SaveChanges();
                        }
                        catch (OptimisticConcurrencyException)
                        {
                            ((IObjectContextAdapter)datacontext)
            .ObjectContext
            .Refresh(RefreshMode.StoreWins, type_id);
                            datacontext.SaveChanges();
                        }
                        objOutput.err_code = 250;
                        objOutput.err_msg = "Question and product deleted successfully.";
                    }
                    else
                    {
                        //TempData["questionmsg"] = "Question details deleted successfully.";
                        objOutput.err_code = 200;
                        objOutput.err_msg = "Question deleted successfully.";
                    }
                }
                else
                {
                    var id = datacontext.tbl_question_master.Where(k => k.question_id == q_id && k.Audit_type_id == subAuditType_id && k.module_id == m_id).FirstOrDefault();
                    datacontext.Entry(id).State = EntityState.Deleted;

                    // tblQuest.question = question;
                    //datacontext.tbl_question_master.Add(tblQuest);
                    datacontext.SaveChanges();
                    tblQuest = datacontext.tbl_question_master.Where(k => k.Audit_type_id == subAuditType_id && k.module_id == m_id).FirstOrDefault();
                    if (tblQuest == null)
                    {
                        var type_id = datacontext.tbl_audit_type.Where(k => k.ID == subAuditType_id && k.module_id == m_id).FirstOrDefault();

                        datacontext.Entry(type_id).State = EntityState.Deleted;
                        try
                        {
                            datacontext.SaveChanges();
                        }
                        catch (OptimisticConcurrencyException)
                        {
                            ((IObjectContextAdapter)datacontext)
            .ObjectContext
            .Refresh(RefreshMode.StoreWins, type_id);
                            datacontext.SaveChanges();
                        }
                        objOutput.err_code = 250;
                        objOutput.err_msg = "Question and product deleted successfully.";
                    }
                    else
                    {
                        //TempData["questionmsg"] = "Question details deleted successfully.";
                        objOutput.err_code = 200;
                        objOutput.err_msg = "Question deleted successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                objOutput.err_code = 202;
                objOutput.err_msg = ex.Message;
                CommonFunctions.ErrorLog(ex);
            }
            return Json(objOutput, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveQuestion(string module_id, string subAudit, string question, string parts, string question_id)
        {
            List<QuestionsPOCO> listquestion = new List<QuestionsPOCO>();
            OutputPOCO objOutput = new OutputPOCO();
            try
            {
                tbl_question_master tblQuest = new tbl_question_master();
                if (!String.IsNullOrEmpty(question_id))
                {
                    if (Convert.ToInt32(question_id) > 0)
                    {
                        int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        int q_id = Convert.ToInt32(question_id);
                        int m_id = Convert.ToInt32(module_id);
                        tblQuest = datacontext.tbl_question_master.AsQueryable().
                        FirstOrDefault(k => k.question_id == q_id && k.Audit_type_id == subAuditType_id && k.module_id == m_id);
                        if (tblQuest != null)
                        {
                            tblQuest.question = question;
                            //datacontext.tbl_question_master.Add(tblQuest);
                            datacontext.SaveChanges();
                            //TempData["questionmsg"] = "Question details updated successfully.";
                            objOutput.audit_type_id = subAuditType_id;
                            objOutput.err_code = 200;
                            objOutput.err_msg = "Question updated successfully.";
                        }
                    }
                }
                else
                {
                    if (Convert.ToInt32(module_id) > 0)
                    {
                        int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        if (subAuditType_id == 0)
                        {
                            tbl_audit_type objtype = new tbl_audit_type();
                            objtype.module_id = Convert.ToInt32(module_id);
                            objtype.Audit_Type = subAudit;
                            if (Convert.ToInt32(module_id) == 3)
                            {
                                objtype.Parts = parts;
                            }
                            datacontext.tbl_audit_type.Add(objtype);
                            datacontext.SaveChanges();
                            subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        }
                        else if (Convert.ToInt32(module_id) == 3)
                        {
                            int mod_id = Convert.ToInt32(module_id);
                            tbl_audit_type tbltype = new tbl_audit_type();
                            tbltype = datacontext.tbl_audit_type.AsQueryable().
                                FirstOrDefault(k => k.module_id == mod_id && k.ID == subAuditType_id);
                            if (tbltype != null)
                            {
                                tbl_audit_type tblpart = new tbl_audit_type();
                                tblpart = datacontext.tbl_audit_type.AsQueryable().
                                    FirstOrDefault(k => k.module_id == mod_id && k.ID == subAuditType_id && k.Parts == parts.Trim());

                                tbltype.Parts = parts.Trim();
                                datacontext.SaveChanges();
                                //TempData["questionmsg"] = "Part details are updated successfully.";
                                objOutput.audit_type_id = subAuditType_id;
                                objOutput.err_code = 200;
                                objOutput.err_msg = "Part details are updated successfully.";
                            }
                            else
                            {
                                objOutput.audit_type_id = subAuditType_id;
                                objOutput.err_code = 200;
                                objOutput.err_msg = "No Product is found for update. Please try again.";
                                //TempData["questionmsg"] = "No Product is found for update. Please try again.";
                            }
                        }
                        if (subAuditType_id > 0)
                        {
                            int Maxquest = objModel.getMaxQusetionId(subAuditType_id, Convert.ToInt32(module_id));
                            if (Maxquest < 1000)
                            {
                                Maxquest = 1001;
                            }
                            else
                            {
                                Maxquest += 1;
                            }
                            tbl_question_master objquest = new tbl_question_master();
                            objquest.Audit_type_id = subAuditType_id;
                            objquest.module_id = Convert.ToInt32(module_id);
                            objquest.question = Convert.ToString(question).Trim();
                            objquest.question_id = Maxquest;
                            datacontext.tbl_question_master.Add(objquest);
                            datacontext.SaveChanges();
                            //TempData["questionmsg"] = "Question with audit type details added successfully.";
                            objOutput.audit_type_id = subAuditType_id;
                            objOutput.err_code = 200;
                            objOutput.err_msg = "Question with audit type details added successfully.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objOutput.err_code = 101;
                objOutput.err_msg = "Error in updating the Question details. Please try again.";
                //TempData["questionmsg"] = "Error in updating the Question details. Please try again.";
                CommonFunctions.ErrorLog(ex);
            }
            return Json(objOutput, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// author:Monika M S
        /// date:2020-10-08
        /// </summary>
        /// <param name="module_id"></param>
        /// <param name="subAudit"></param>
        /// <param name="inputtype"></param>
        /// <param name="inputval"></param>
        /// <param name="question"></param>
        /// <param name="parts"></param>
        /// <param name="question_id"></param>
        /// <returns>Inserting the question with answer type in Json Format</returns>
        [HttpPost]
        public ActionResult SaveQuestionWithJson(string module_id, string subAudit, string inputtype, string inputval, string question, string parts, string question_id, int freqId,string ncval)
        {
            List<QuestionsPOCO> listquestion = new List<QuestionsPOCO>();
            OutputPOCO objOutput = new OutputPOCO();
            string choice = "";
            int inputvalue = 0;
            var ansval = "";
            string inputJson = "";
            string JsonData = "";
            string inputvalue1 = "";
            try
            {
                if (question_id == "0")
                {
                    question_id = "";
                }
                if (inputval != "")
                {
                    inputvalue = Convert.ToInt32(inputval);
                    inputvalue1 = Convert.ToString(inputvalue);
                    ansval = datacontext.tbl_InputValue.Where(m => m.ID == inputvalue).Select(m => m.InputValue).FirstOrDefault();
                    if (ansval != "")
                    {
                        string[] ansvallist = ansval.Split('/');
                        //var ansvallist = datacontext.tbl_InputValue.Where(m => m.inputId == inputType).ToList();

                        for (int i = 0; i < ansvallist.Length; i++)
                        {
                            int j = i + 1;
                            choice += "\"choice" + j + "\":\"" + ansvallist[i] + "\",";
                        }
                        choice = choice.Remove(choice.Length - 1);
                    }
                    else
                    {
                        choice = "\"choice1\":\"\"";
                    }
                }

                int inputType = Convert.ToInt32(inputtype);

                var anstype = datacontext.tbl_inputtype.Where(m => m.Id == inputType).Select(m => m.inputType).FirstOrDefault();

                inputJson = "[{\"Answer_type\":\"" + anstype + "\",\"ans_value\":\"" + ansval + "\"," + choice + "}]";
                JsonData = "[{\"question_id\":\"" + question_id + "\",\"question\":\"" + question + "\",\"Answer_type\":\"" + anstype + "\",\"ans_value\":\"" + ansval + "\" }]";

            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }

            try
            {
                tbl_Question_TypeWithJson tblQuest = new tbl_Question_TypeWithJson();
                if (!String.IsNullOrEmpty(question_id))
                {
                    if (Convert.ToInt32(question_id) > 0)
                    {
                        int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        int q_id = Convert.ToInt32(question_id);
                        int m_id = Convert.ToInt32(module_id);
                        tblQuest = datacontext.tbl_Question_TypeWithJson.AsQueryable().
                        FirstOrDefault(k => k.Question_Id == q_id && k.Audit_type_id == subAuditType_id && k.module_id == m_id);
                        if (tblQuest != null)
                        {
                            tblQuest.Question = question;
                            tblQuest.InputType = inputtype;
                            tblQuest.InputValue = inputvalue1;
                            tblQuest.module_id = Convert.ToInt32(module_id);
                            tblQuest.Audit_type_id = subAuditType_id;
                            tblQuest.JsonFormat = JsonData;
                            tblQuest.FrequencyId = freqId;
                            tblQuest.inputJson = inputJson;
                            tblQuest.NCValue = ncval;
                            //datacontext.tbl_question_master.Add(tblQuest);
                            datacontext.SaveChanges();
                            //TempData["questionmsg"] = "Question details updated successfully.";
                            objOutput.audit_type_id = subAuditType_id;
                            objOutput.err_code = 200;
                            objOutput.err_msg = "Question updated successfully.";
                        }
                    }
                }
                else
                {
                    if (Convert.ToInt32(module_id) > 0)
                    {
                        int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        if (subAuditType_id == 0)
                        {
                            tbl_audit_type objtype = new tbl_audit_type();
                            objtype.module_id = Convert.ToInt32(module_id);
                            objtype.Audit_Type = subAudit;
                            if (Convert.ToInt32(module_id) == 3)
                            {
                                objtype.Parts = parts;
                            }
                            datacontext.tbl_audit_type.Add(objtype);
                            datacontext.SaveChanges();
                            subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        }
                        else if (Convert.ToInt32(module_id) == 3)
                        {
                            int mod_id = Convert.ToInt32(module_id);
                            tbl_audit_type tbltype = new tbl_audit_type();
                            tbltype = datacontext.tbl_audit_type.AsQueryable().
                                FirstOrDefault(k => k.module_id == mod_id && k.ID == subAuditType_id);
                            if (tbltype != null)
                            {
                                tbl_audit_type tblpart = new tbl_audit_type();
                                tblpart = datacontext.tbl_audit_type.AsQueryable().
                                    FirstOrDefault(k => k.module_id == mod_id && k.ID == subAuditType_id && k.Parts == parts.Trim());

                                tbltype.Parts = parts.Trim();
                                datacontext.SaveChanges();
                                //TempData["questionmsg"] = "Part details are updated successfully.";
                                objOutput.audit_type_id = subAuditType_id;
                                objOutput.err_code = 200;
                                objOutput.err_msg = "Part details are updated successfully.";
                            }
                            else
                            {
                                objOutput.audit_type_id = subAuditType_id;
                                objOutput.err_code = 200;
                                objOutput.err_msg = "No Product is found for update. Please try again.";
                                //TempData["questionmsg"] = "No Product is found for update. Please try again.";
                            }
                        }
                        if (subAuditType_id > 0)
                        {
                            int Maxquest = objModel.getMaxQusetionIdForAnswerType(subAuditType_id, Convert.ToInt32(module_id));

                            tbl_Question_TypeWithJson objquest = new tbl_Question_TypeWithJson();
                            objquest.Audit_type_id = subAuditType_id;
                            objquest.module_id = Convert.ToInt32(module_id);
                            objquest.Question = Convert.ToString(question).Trim();
                            objquest.Question_Id = Maxquest;
                            objquest.InputType = inputtype;
                            objquest.InputValue = inputvalue1;
                            objquest.JsonFormat = JsonData;
                            objquest.FrequencyId = freqId;
                            objquest.NCValue = ncval;
                            objquest.inputJson = inputJson;
                            datacontext.tbl_Question_TypeWithJson.Add(objquest);
                            datacontext.SaveChanges();
                            //TempData["questionmsg"] = "Question with audit type details added successfully.";
                            objOutput.audit_type_id = subAuditType_id;
                            objOutput.err_code = 200;
                            objOutput.err_msg = "Question with audit type details added successfully.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objOutput.err_code = 101;
                objOutput.err_msg = "Error in updating the Question details. Please try again.";
                //TempData["questionmsg"] = "Error in updating the Question details. Please try again.";
                CommonFunctions.ErrorLog(ex);
            }
            return Json(objOutput, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// author:Monika M S
        /// date:2020-10-08
        /// </summary>
        /// <param name="module_id"></param>
        /// <returns>get the audit name based on module id</returns>
        public string GetModuleName(int module_id)
        {
            string res = "";
            try
            {
                var modulename = datacontext.tbl_Module_master.Where(m => m.module_id == module_id).Select(m => m.module).FirstOrDefault();
                res = modulename;
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return res;
        }

        /// <summary>
        /// author:Monika M S
        /// date:2020-10-09
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>get the saved question details for edit </returns>
        public JsonResult GetQuestionDetails(int QuesId, string module_id, string subAudit)
        {
            var questiondetails = new tbl_Question_TypeWithJson();
            try
            {
                int modid = Convert.ToInt32(module_id);
                int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                if (subAuditType_id != 0)
                {
                    var questiondet = datacontext.tbl_Question_TypeWithJson.Where(m => m.Question_Id == QuesId && m.module_id == modid && m.Audit_type_id == subAuditType_id).Select(m => new { m.InputType, m.InputValue, m.NCValue }).FirstOrDefault();
                    if (questiondet != null)
                    {
                        questiondetails.InputType = questiondet.InputType;
                        questiondetails.InputValue = questiondet.InputValue;
                        questiondetails.NCValue = questiondet.NCValue;
                    }
                }
            }
            catch(Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }

            return Json(questiondetails, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult GetQuestionList(QuestionsPOCO obj)
        //{
        //    List<QuestionsPOCO> listquestion = new List<QuestionsPOCO>();
        //    try
        //    {
        //        listquestion = (from m in datacontext.sp_getQuestionList(Convert.ToInt32(obj.module_id), Convert.ToInt32(obj.Audit_type_id), Convert.ToInt32(obj.section_id), 0)
        //                        select new QuestionsPOCO
        //                        {
        //                            question_id = m.question_id,
        //                            question = m.question,
        //                            Audit_type_id = Convert.ToString(m.audit_type_id),
        //                            AuditType = m.Audit_Type
        //                        }).ToList();

        //        return Json(listquestion, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        //TempData["questionmsg"] = "Error in updating the Question details. Please try again.";
        //        CommonFunctions.ErrorLog(ex);
        //        return Json(true);
        //    }

        //}

        public PartialViewResult Clear()
        {
            ModelState.Clear();
            return PartialView("DisplayQuestion");
        }
        public PartialViewResult EditQuestion(string viewName)
        {
            QuestionsPOCO obj = new QuestionsPOCO();
            ViewData["inputtype"] = new SelectList(datacontext.tbl_inputtype, "ID", "inputType").ToList();
            return PartialView(viewName, obj);
        }

        /// <summary>
        /// author:Monika M S
        /// date:2020-10-08
        /// </summary>
        /// <param name="inputID"></param>
        /// <returns>Get all the input values to load inputvalue dropdown</returns>
        public JsonResult GetInputValue(int inputID)
        {
               var DeptData = (from row in datacontext.tbl_InputValue
                                where row.inputId == inputID
                                select new { Value = row.ID, Text = row.InputValue }
                                     ).ToList();
                return Json(DeptData, JsonRequestBehavior.AllowGet);
           
        }
        public PartialViewResult DisplayQuestion(QuestionsPOCO obj)
        {
            List<QuestionsPOCO> listquestion = new List<QuestionsPOCO>();
            try
            {
                ViewData["inputtype"] = new SelectList(datacontext.tbl_inputtype, "ID", "inputType").ToList();
                int sec = Convert.ToInt32(HttpContext.Session["SectorId"]);
                int freq = Convert.ToInt32(obj.FrequencyId);
                Session["ImportExcel"] = "import";
                Session["audittypeid"] = "1";
                Session["moduleid"] = "1";
                Session["Subauditval"] = "1";
                Session["Successmsg"] = null;
                Session["error"] = null;
                listquestion = (from m in datacontext.sp_getQuestionList(Convert.ToInt32(obj.module_id), Convert.ToInt32(obj.Audit_type_id), 0, sec, freq)
                                select new QuestionsPOCO
                                {
                                    question_id = m.question_id,
                                    question = m.question,
                                    Audit_type_id = Convert.ToString(m.audit_type_id),
                                    AuditType = m.Audit_Type
                                }).ToList();

            }
            catch (Exception ex)
            {
                //TempData["questionmsg"] = "Error in updating the Question details. Please try again.";
                CommonFunctions.ErrorLog(ex);
            }

            return PartialView("DisplayQuestion", listquestion);
        }

        /// <summary>
        /// author:Monika M S
        /// date:2020-09-01
        /// </summary>
        /// <returns>Download the New Excel</returns>
        public ActionResult DownloadTemplate()
        {

            #region Excel and Stuff
            try
            {
                DateTime frda = DateTime.Now;
                string ReportList = null, Template = null;
                ExcelPackage templatep = null;
                int sectorid = Convert.ToInt32(HttpContext.Session["SectorId"]);
                ReportList = ConfigurationManager.AppSettings["DownloadExcelForQuestionReportList"];
                FileInfo newFile = new FileInfo(System.IO.Path.Combine(ReportList, "UploadQuestionExport" + frda.ToString("yyyy-MM-dd") + ".xlsx"));
                if (sectorid == 3)
                {
                    Template = ConfigurationManager.AppSettings["DownloadExcelForSustainability"];
                    //String FileDir = @"C:\AudireReportTemplate\ReportsList\";
                    // TODO Handle an empty string, populate listview with disk drives
                    bool exists = System.IO.Directory.Exists(ReportList);
                    if (!exists)
                        System.IO.Directory.CreateDirectory(ReportList);

                    if (newFile.Exists)
                    {
                        try
                        {
                            newFile.Delete();  // ensures we create a new workbook
                                               // TODO Handle an empty string, populate listview with disk drives
                            newFile = new FileInfo(System.IO.Path.Combine(ReportList, "UploadQuestionExport" + frda.ToString("yyyy-MM-dd") + ".xlsx")); //" to " + toda.ToString("yyyy-MM-dd") + 
                        }
                        catch
                        {
                            TempData["Excelopen"] = "Excel with same date is already open, please close it and try to generate!!!!";
                            //return View();
                        }
                    }
                    FileInfo templateFile = new FileInfo(Template);
                    //FileInfo templateFile = new FileInfo(@"C:\AudireReportTemplate\MainTemplate\UploadQuestionExport.xlsx");
                    templatep = new ExcelPackage(templateFile);
                }
                else
                {
                    Template = ConfigurationManager.AppSettings["DownloadExcelForQuestionTemplate"];
                    //String FileDir = @"C:\AudireReportTemplate\ReportsList\";
                    // TODO Handle an empty string, populate listview with disk drives
                    bool exists = System.IO.Directory.Exists(ReportList);
                    if (!exists)
                        System.IO.Directory.CreateDirectory(ReportList);

                    if (newFile.Exists)
                    {
                        try
                        {
                            newFile.Delete();  // ensures we create a new workbook
                                               // TODO Handle an empty string, populate listview with disk drives
                            newFile = new FileInfo(System.IO.Path.Combine(ReportList, "UploadQuestionExport" + frda.ToString("yyyy-MM-dd") + ".xlsx")); //" to " + toda.ToString("yyyy-MM-dd") + 
                        }
                        catch
                        {
                            TempData["Excelopen"] = "Excel with same date is already open, please close it and try to generate!!!!";
                            //return View();
                        }
                    }
                    FileInfo templateFile = new FileInfo(Template);
                    // FileInfo templateFile = new FileInfo(@"C:\AudireReportTemplate\MainTemplate\UploadQuestionExport.xlsx");
                    templatep = new ExcelPackage(templateFile);
                }
                ExcelWorksheet Templatews = templatep.Workbook.Worksheets[1];
                //Using the File for generation and populating it
                ExcelPackage p = null;
                p = new ExcelPackage(newFile);
                ExcelWorksheet worksheet = null;

                //Creating the WorkSheet for populating
                try
                {
                    //worksheet = p.Workbook.Worksheets.Add(System.DateTime.Now.ToString("dd-MM-yyyy"), Templatews);
                    worksheet = p.Workbook.Worksheets.Add("Question", Templatews);
                }
                catch { }

                if (worksheet == null)
                {
                    //worksheet = p.Workbook.Worksheets.Add(System.DateTime.Now.ToString("dd-MM-yyyy"), Templatews);
                    worksheet = p.Workbook.Worksheets.Add("Question", Templatews);
                }
                int sheetcount = p.Workbook.Worksheets.Count;
                p.Workbook.Worksheets.MoveToStart(sheetcount);
                worksheet.Cells.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                //worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                p.Save();
                #endregion

                #region Save and Download

                //Downloding Excel
                string ToDate = frda.ToString("yyyy-MM-dd");
                string path1 = System.IO.Path.Combine(ReportList, "UploadQuestionExport" + frda.ToString("yyyy-MM-dd") + ".xlsx");
                DownloadReport(path1, "UploadQuestionExport", ToDate);
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            #endregion
            return View();
        }
        /// <summary>
        /// author:Monika M S
        /// Date:2020-09-01
        /// Description: To Download the Excel file
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="FileName"></param>
        /// <param name="ToDate"></param>
        public void DownloadReport(String FilePath, String FileName, String ToDate)
        {
            System.IO.FileInfo file1 = new System.IO.FileInfo(FilePath);
            //string Outgoingfile = FileName + Convert.ToDateTime(ToDate).ToString("yyyy-MM-dd") + ".xlsx";
            string Outgoingfile = FileName + Convert.ToDateTime(ToDate).ToString("yyyy-MM-dd") + ".xlsx";
            if (file1.Exists)
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Outgoingfile);
                Response.AddHeader("Content-Length", file1.Length.ToString());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.WriteFile(file1.FullName);
                Response.Flush();
                Response.Close();
                Response.End();
            }
        }

        public void GetmoduleIdAndSubModuleId(string moduleId, string submoduleID, string auditName)
        {
            Session["moduleId"] = moduleId;
            Session["subModuleId"] = submoduleID;
            Session["AuditName"] = auditName;
        }

        /// <summary>
        /// author:Monika M S
        /// date:2020-09-01
        /// </summary>
        /// <param name="audittypeid"></param>
        /// <param name="moduleid"></param>
        /// <returns>If we write new sub audit type then that sub audit type will insert into audit type table </returns>
        public ActionResult InsertAuditTypeID(string audittypeid, int moduleid)
        {
            int dis_seq = 0;
            QuestionsPOCO objques = new QuestionsPOCO();
            try
            {
                var auditdatapresentOrnot = datacontext.tbl_audit_type.Where(m => m.module_id == moduleid && m.Audit_Type == audittypeid).FirstOrDefault();
                if (auditdatapresentOrnot == null)
                {
                    tbl_audit_type objaudit = new tbl_audit_type();
                    objaudit.Audit_Type = audittypeid;
                    objaudit.module_id = moduleid;
                    objaudit.Status = 1;
                    var displayseq = datacontext.tbl_audit_type.Where(m => m.module_id == moduleid).OrderByDescending(m => m.ID).Select(m => m.display_seq).FirstOrDefault();
                    if (displayseq == null)
                    {
                        dis_seq = 1;
                    }
                    else
                    {
                        dis_seq = (int)displayseq + 1;
                    }
                    objaudit.display_seq = dis_seq;
                    datacontext.tbl_audit_type.Add(objaudit);
                    datacontext.SaveChanges();

                    objques.module_id = Convert.ToString(moduleid);
                    objques.Audit_type_id = Convert.ToString(objaudit.ID);

                }
                else
                {
                    Session["error"] = "The audit Name already exists";
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            finally
            {
                objques = new QuestionsPOCO();
            }
            return RedirectToAction("Questions", objques);
        }

        /// <summary>
        /// author:Monika M S
        /// Date:2020-09-01
        /// </summary>
        /// <param name="selectedfile"></param>
        /// <returns>To Import the data from excel to database</returns>
        [HttpPost]
        public ActionResult ImportQuestions(HttpPostedFileBase selectedfile)
        {
            Session["error"] = null;
            OutputPOCO objOutput = new OutputPOCO();
            QuestionsPOCO objques = new QuestionsPOCO();
            int sectorid = Convert.ToInt32(HttpContext.Session["SectorId"]);
            try
            {

                if (sectorid == 3)
                {
                    #region
                    string moduleid = null, auditid = null;
                    int modid = 0, audid = 0;
                    string fileLocation1 = Server.MapPath("~/Content/");
                    DirectoryInfo di = new DirectoryInfo(fileLocation1);
                    FileInfo[] files = di.GetFiles("*.xlsx").Where(p => p.Extension == ".xlsx").ToArray();
                    foreach (FileInfo file1 in files)
                        try
                        {
                            file1.Attributes = FileAttributes.Normal;
                            System.IO.File.Delete(file1.FullName);
                        }
                        catch { }
                    #endregion

                    DataSet ds = new DataSet();
                    if (Request.Files["selectedfile"].ContentLength > 0)
                    {
                        if (Request.Files["selectedfile"].FileName.Contains("UploadQuestionExport"))
                        {
                            string fileExtension = System.IO.Path.GetExtension(Request.Files["selectedfile"].FileName);
                            if (fileExtension == ".xls" || fileExtension == ".xlsx")
                            {
                                string fileLocation = Server.MapPath("~/Content/") + Request.Files["selectedfile"].FileName;
                                if (System.IO.File.Exists(fileLocation))
                                {
                                    System.IO.File.Delete(fileLocation);
                                }
                                Request.Files["selectedfile"].SaveAs(fileLocation);
                                string excelConnectionString = string.Empty;
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                //connection String for xls file format.
                                if (fileExtension == ".xls")
                                {
                                    excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                    fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                                }
                                //connection String for xlsx file format.
                                else if (fileExtension == ".xlsx")
                                {
                                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                }
                                //Create Connection to Excel work book and add oledb namespace
                                OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                                excelConnection.Open();
                                DataTable dt = new DataTable();

                                dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                                if (dt == null)
                                {
                                    return null;
                                }
                                String[] excelSheets = new String[dt.Rows.Count];
                                int t = 0;
                                //excel data saves in temp file here.
                                foreach (DataRow row in dt.Rows)
                                {

                                    excelSheets[t] = row["TABLE_NAME"].ToString();
                                    t++;
                                }
                                OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                                string query = string.Format("Select * from [{0}]", excelSheets[0]);
                                using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                                {
                                    dataAdapter.Fill(ds);
                                }
                                excelConnection.Close();
                                excelConnection1.Close();
                            }
                            moduleid = Convert.ToString(Session["moduleId"]);
                            auditid = Convert.ToString(Session["subModuleId"]);
                            if (auditid != "")
                            {
                                var subauditid = datacontext.tbl_audit_type.Where(m => m.Audit_Type == auditid).Select(m => m.ID).FirstOrDefault();
                                audid = Convert.ToInt32(subauditid);
                            }
                            modid = Convert.ToInt32(moduleid);
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                tbl_Question_TypeWithJson objquesMaster = new tbl_Question_TypeWithJson();
                                string sector = null, helptext = null, auditname = null, subauditname = null, frequency = null, choice = "", anwerType = null, answerValue = null, question = null;
                                int? questionId = 0, inswerId = 0, insvalid = 0, module_id = 0, subauditid = 0, sectorId = 0;
                                sector = Convert.ToString(ds.Tables[0].Rows[i][0]);
                                auditname = Convert.ToString(ds.Tables[0].Rows[i][1]);
                                subauditname = Convert.ToString(ds.Tables[0].Rows[i][2]);
                                frequency = Convert.ToString(ds.Tables[0].Rows[i][3]);
                                question = Convert.ToString(ds.Tables[0].Rows[i][5]);
                                if (question != "")
                                {
                                    questionId = Convert.ToInt32(ds.Tables[0].Rows[i][4]);
                                    helptext = Convert.ToString(ds.Tables[0].Rows[i][6]);
                                    anwerType = Convert.ToString(ds.Tables[0].Rows[i][7]);
                                    answerValue = Convert.ToString(ds.Tables[0].Rows[i][8]);

                                    var ansertypedet = datacontext.tbl_inputtype.Where(m => m.inputType == anwerType).FirstOrDefault();
                                    if (ansertypedet == null)
                                    {
                                        tbl_inputtype objinput = new tbl_inputtype();
                                        objinput.inputType = anwerType;
                                        datacontext.tbl_inputtype.Add(objinput);
                                        datacontext.SaveChanges();
                                        inswerId= datacontext.tbl_inputtype.Where(m => m.inputType == anwerType).Select(m=>m.Id).FirstOrDefault();
                                    }
                                    else
                                    {
                                        inswerId = ansertypedet.Id;
                                    }
                                    var anservaldet = datacontext.tbl_InputValue.Where(m => m.InputValue == answerValue).FirstOrDefault();
                                    if (anservaldet == null)
                                    {
                                        tbl_InputValue objinput = new tbl_InputValue();
                                        objinput.InputValue = answerValue;
                                        objinput.inputId = datacontext.tbl_inputtype.Where(m => m.inputType == anwerType).Select(m => m.Id).FirstOrDefault();
                                        datacontext.tbl_InputValue.Add(objinput);
                                        datacontext.SaveChanges();

                                        insvalid = datacontext.tbl_InputValue.Where(m => m.InputValue == answerValue).Select(m => m.ID).FirstOrDefault();

                                        var anservaldet1 = datacontext.tbl_InputValue.Where(m => m.InputValue == answerValue).FirstOrDefault();
                                        string[] ansvallist = anservaldet1.InputValue.Split('/');
                                        //var ansvallist = datacontext.tbl_InputValue.Where(m => m.inputId == inputType).ToList();

                                        for (int j = 0; j < ansvallist.Length; j++)
                                        {
                                            int k = j + 1;
                                            choice += "\"choice" + k + "\":\"" + ansvallist[k-1] + "\",";
                                        }
                                        choice = choice.Remove(choice.Length - 1);
                                    }
                                    else
                                    {
                                        insvalid = anservaldet.ID;

                                        string[] ansvallist = anservaldet.InputValue.Split('/');
                                        //var ansvallist = datacontext.tbl_InputValue.Where(m => m.inputId == inputType).ToList();

                                        for (int j = 0; j < ansvallist.Length; j++)
                                        {
                                            int k = j + 1;
                                            choice += "\"choice" + k + "\":\"" + ansvallist[k-1] + "\",";
                                        }
                                        choice = choice.Remove(choice.Length - 1);
                                    }

                                    var sectordet = datacontext.tbl_sector_master.Where(m => m.Sector_Name == sector).FirstOrDefault();
                                    if (sectordet == null)
                                    {
                                        tbl_sector_master obj = new tbl_sector_master();
                                        obj.Sector_Name = sector;
                                        datacontext.tbl_sector_master.Add(obj);
                                        datacontext.SaveChanges();
                                    }

                                    var moduledet = datacontext.tbl_Module_master.Where(m => m.module == auditname).FirstOrDefault();
                                    if (moduledet == null)
                                    {
                                        tbl_Module_master obj = new tbl_Module_master();
                                        obj.module = auditname;
                                        obj.Sector = datacontext.tbl_sector_master.Where(m => m.Sector_Name == sector).Select(m => m.ID).FirstOrDefault();
                                        obj.status = 1;
                                        datacontext.tbl_Module_master.Add(obj);
                                        datacontext.SaveChanges();
                                        module_id = datacontext.tbl_Module_master.Where(m => m.module == auditname).Select(m=>m.module_id).FirstOrDefault();
                                    }
                                    else
                                    {
                                        module_id = moduledet.module_id;
                                    }
                                    var subauditdet = datacontext.tbl_audit_type.Where(m => m.Audit_Type == subauditname).FirstOrDefault();
                                    if (subauditdet == null)
                                    {
                                        tbl_audit_type obj = new tbl_audit_type();
                                        obj.Audit_Type = subauditname;
                                        obj.module_id = datacontext.tbl_Module_master.Where(m => m.module == auditname).Select(m => m.module_id).FirstOrDefault();
                                        obj.Status = 1;
                                        obj.Color_code = "#283747";
                                        obj.display_seq = 2;
                                        datacontext.tbl_audit_type.Add(obj);
                                        datacontext.SaveChanges();
                                        subauditid= datacontext.tbl_audit_type.Where(m => m.Audit_Type == subauditname).Select(m=>m.ID).FirstOrDefault();
                                    }
                                    else
                                    {
                                        subauditid = subauditdet.ID;
                                    }

                                    var freqdet = datacontext.tbl_frequency_master.Where(m => m.Name == frequency).FirstOrDefault();
                                    if (freqdet == null)
                                    {
                                        tbl_frequency_master obj = new tbl_frequency_master();
                                        obj.Name = frequency;
                                        datacontext.tbl_frequency_master.Add(obj);
                                        datacontext.SaveChanges();
                                    }
                                    string inputJson = "[{'Answer_type':\"" + anwerType + "\",'ans_value':\"" + answerValue + "\",\"" + choice + "\"}]";
                                    string JsonData = "[{'question_id':\"" + questionId + "\",'question':\"" + question + "\",'Answer_type:\"" + anwerType + "\",'ans_value':\"" + answerValue + "\" }]";


                                    var questiondet = datacontext.tbl_Question_TypeWithJson.Where(m => m.module_id == module_id && m.Audit_type_id == subauditid && m.Question_Id == questionId).FirstOrDefault();
                                    if (questiondet == null)
                                    {
                                        objquesMaster.Question = question;
                                        objquesMaster.Question_Id = (int)questionId;
                                        objquesMaster.InputType = Convert.ToString(inswerId);
                                        objquesMaster.InputValue = Convert.ToString(insvalid);
                                        objquesMaster.FrequencyId = datacontext.tbl_frequency_master.Where(m => m.Name == frequency).Select(m => m.FreaquencyId).FirstOrDefault(); ;
                                        objquesMaster.module_id = datacontext.tbl_Module_master.Where(m => m.module == auditname).Select(m => m.module_id).FirstOrDefault(); ;
                                        objquesMaster.sectorId = datacontext.tbl_sector_master.Where(m => m.Sector_Name == sector).Select(m => m.ID).FirstOrDefault(); ;
                                        objquesMaster.JsonFormat = JsonData;
                                        objquesMaster.inputJson = inputJson;
                                        objquesMaster.Audit_type_id = datacontext.tbl_audit_type.Where(m => m.Audit_Type == subauditname).Select(m => m.ID).FirstOrDefault(); ;
                                        objquesMaster.HelpText = helptext;
                                        datacontext.tbl_Question_TypeWithJson.Add(objquesMaster);
                                        datacontext.SaveChanges();
                                        objOutput.audit_type_id = audid;
                                        objOutput.err_code = 200;
                                        Session["Successmsg"] = "Question imported successfully.";

                                        Session["ImportExcel"] = "Success";
                                        Session["moduleid"] = modid;
                                        Session["audittypeid"] = audid;
                                        Session["Subauditval"] = auditid;
                                    }
                                    else
                                    {
                                        questiondet.Question = question;
                                        questiondet.InputType = Convert.ToString(inswerId);
                                        questiondet.InputValue = Convert.ToString(insvalid);
                                        questiondet.FrequencyId = datacontext.tbl_frequency_master.Where(m => m.Name == frequency).Select(m => m.FreaquencyId).FirstOrDefault(); ;
                                        questiondet.sectorId = datacontext.tbl_sector_master.Where(m => m.Sector_Name == sector).Select(m => m.ID).FirstOrDefault(); ;
                                        questiondet.JsonFormat = JsonData;
                                        questiondet.inputJson = inputJson;
                                        questiondet.HelpText = helptext;
                                        datacontext.SaveChanges();
                                        Session["Successmsg"] = "Question imported successfully.";

                                        Session["ImportExcel"] = "Success";
                                        Session["moduleid"] = modid;
                                        Session["audittypeid"] = audid;
                                        Session["Subauditval"] = auditid;
                                    }
                                }
                                else
                                {
                                    Session["error"] = "Please Attach Valid Excel Which Contain Questions";
                                }
                            }
                        }
                        else
                        {
                            Session["error"] = "Please Attach Valid Excel.";
                        }
                    }
                }
                else
                {

                    #region
                    string moduleid = null, auditid = null;
                    int modid = 0, audid = 0;
                    string fileLocation1 = Server.MapPath("~/Content/");
                    DirectoryInfo di = new DirectoryInfo(fileLocation1);
                    FileInfo[] files = di.GetFiles("*.xlsx").Where(p => p.Extension == ".xlsx").ToArray();
                    foreach (FileInfo file1 in files)
                        try
                        {
                            file1.Attributes = FileAttributes.Normal;
                            System.IO.File.Delete(file1.FullName);
                        }
                        catch { }
                    #endregion
                    DataSet ds = new DataSet();
                    if (Request.Files["selectedfile"].ContentLength > 0)
                    {
                        if (Request.Files["selectedfile"].FileName.Contains("UploadQuestionExport"))
                        {
                            string fileExtension = System.IO.Path.GetExtension(Request.Files["selectedfile"].FileName);
                            if (fileExtension == ".xls" || fileExtension == ".xlsx")
                            {
                                string fileLocation = Server.MapPath("~/Content/") + Request.Files["selectedfile"].FileName;
                                if (System.IO.File.Exists(fileLocation))
                                {
                                    System.IO.File.Delete(fileLocation);
                                }
                                Request.Files["selectedfile"].SaveAs(fileLocation);
                                string excelConnectionString = string.Empty;
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                                                fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                //connection String for xls file format.
                                if (fileExtension == ".xls")
                                {
                                    excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                                    fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                                }
                                //connection String for xlsx file format.
                                else if (fileExtension == ".xlsx")
                                {
                                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                                    fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                }
                                //Create Connection to Excel work book and add oledb namespace
                                OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                                excelConnection.Open();
                                DataTable dt = new DataTable();

                                moduleid = Convert.ToString(Session["moduleId"]);
                                auditid = Convert.ToString(Session["subModuleId"]);
                                if (auditid != "")
                                {
                                    var subauditid = datacontext.tbl_audit_type.Where(m => m.Audit_Type == auditid).Select(m => m.ID).FirstOrDefault();
                                    audid = Convert.ToInt32(subauditid);
                                }
                                modid = Convert.ToInt32(moduleid);

                                dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);


                                if (dt == null)
                                {
                                    return null;
                                }
                                String[] excelSheets = new String[dt.Rows.Count];
                                int t = 0;
                                //excel data saves in temp file here.
                                foreach (DataRow row in dt.Rows)
                                {

                                    excelSheets[t] = row["TABLE_NAME"].ToString();
                                    t++;
                                }
                                OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                                string query = string.Format("Select * from [{0}]", excelSheets[0]);
                                using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                                {
                                    dataAdapter.Fill(ds);
                                    ds.Tables[0].Columns.Add("QuestionID", typeof(System.Int32));
                                    var moduleandauditid = datacontext.tbl_question_master.Where(m => m.module_id == modid && m.Audit_type_id == audid).OrderByDescending(m => m.id).FirstOrDefault();
                                    if (moduleandauditid != null)
                                    {
                                        int questionId = moduleandauditid.question_id + 1;

                                        foreach (DataRow dr in ds.Tables[0].Rows)
                                        {
                                            dr["QuestionID"] = questionId;
                                            questionId = questionId + 1;
                                        }
                                    }
                                    else
                                    {
                                        int questionId = 0;

                                        foreach (DataRow dr in ds.Tables[0].Rows)
                                        {
                                            dr["QuestionID"] = questionId;
                                            questionId = questionId + 1;
                                        }
                                    }

                                }
                                excelConnection.Close();
                                excelConnection1.Close();
                            }
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                tbl_question_master objquesMaster = new tbl_question_master();
                                string clause = null, helptext = null, sectionName = null, subsectionName = null, question = null;
                                int? sectionid = 0, subsectionid = 0, questionId = 0;
                                clause = Convert.ToString(ds.Tables[0].Rows[i][4]);
                                question = Convert.ToString(ds.Tables[0].Rows[i][0]);
                                if (question != "")
                                {
                                    questionId = Convert.ToInt32(ds.Tables[0].Rows[i][28]);
                                    helptext = Convert.ToString(ds.Tables[0].Rows[i][3]);
                                    sectionName = Convert.ToString(ds.Tables[0].Rows[i][1]);
                                    subsectionName = Convert.ToString(ds.Tables[0].Rows[i][2]);
                                    var sectionId = datacontext.tbl_section_master.Where(m => m.section_name == sectionName).Select(m => m.section_id).FirstOrDefault();
                                    if (sectionId != 0)
                                    {
                                        sectionid = sectionId;
                                    }
                                    var subsectionId = datacontext.tbl_sub_section_master.Where(m => m.sub_section_name == subsectionName).Select(m => m.sub_section_id).FirstOrDefault();
                                    if (subsectionId != 0)
                                    {
                                        subsectionid = subsectionId;
                                    }
                                    objquesMaster.question = question;
                                    objquesMaster.question_id = (int)questionId;
                                    objquesMaster.section_id = sectionid;
                                    objquesMaster.sub_section_id = subsectionid;
                                    objquesMaster.section_name = sectionName;
                                    objquesMaster.sub_section_name = subsectionName;
                                    objquesMaster.module_id = modid;
                                    objquesMaster.Audit_type_id = audid;
                                    objquesMaster.help_text = helptext;
                                    objquesMaster.clause = clause;
                                    datacontext.tbl_question_master.Add(objquesMaster);
                                    datacontext.SaveChanges();
                                    objOutput.audit_type_id = audid;
                                    objOutput.err_code = 200;
                                    Session["Successmsg"] = "Question imported successfully.";

                                    Session["ImportExcel"] = "Success";
                                    Session["moduleid"] = modid;
                                    Session["audittypeid"] = audid;
                                    Session["Subauditval"] = auditid;
                                }
                                else
                                {
                                    Session["error"] = "Please Attach Valid Excel Which Contain Questions";
                                }
                            }
                        }
                        else
                        {
                            Session["error"] = "Please Attach Valid Excel.";
                        }
                    }

                    objques.module_id = Convert.ToString(modid);
                    objques.Audit_type_id = Convert.ToString(audid);
                }
            }
            catch (Exception ex)
            {
                objOutput.err_code = 101;
                Session["error"] = "Error in importing the Question details. Please try again.";
                CommonFunctions.ErrorLog(ex);
            }

            return RedirectToAction("Questions");
        }
    }
}
