//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Audire_EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_frequency_master
    {
        public tbl_frequency_master()
        {
            this.tbl_Question_TypeWithJson = new HashSet<tbl_Question_TypeWithJson>();
        }
    
        public int FreaquencyId { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<tbl_Question_TypeWithJson> tbl_Question_TypeWithJson { get; set; }
    }
}
